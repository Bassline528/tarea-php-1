<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Tabla productos</title>
</head>
<style>
    .title {
        background-color: yellow;
    }

    .subtitle {
        background-color: grey;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
    
</style>
<body>

<?php
class Product {
    private $name;
    private $amount;
    private $price;
    
    public function __construct( $name, $amount, $price) {
        $this->name = $name;
        $this->amount = $amount;
        $this->price = $price;
    }


    public function getName() {
        return $this->name;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function getPrice() {
        return $this->price;
    }
}

$productsList = array();

$productsList[0] = new Product("Coca Cola", "100", "4.500");
$productsList[1] = new Product("Pepsi", "30", "4.800");
$productsList[2] = new Product("Sprite", "20", "4.500");
$productsList[3] = new Product("Guarana", "200", "4.500");
$productsList[4] = new Product("SevenUp", "24", "4.800");
$productsList[5] = new Product("Mirinda Guarana", "89", "4.800");
$productsList[6] = new Product("Mirinda Naranja", "56", "4.800");
$productsList[7] = new Product("Fanta Naranja", "10", "4.500");
$productsList[8] = new Product("Fanta piña", "2", "4.500");



?> 
<table>
<caption class="title">Productos</caption>
  <tr>
    <th class="subtitle">Company</th>
    <th class="subtitle">Contact</th>
    <th class="subtitle">Country</th>
  </tr>
  <?php foreach($productsList as $product) { ?>
	<tr>
      <td style="text-align:left"> <?php echo ($product->getName()); ?></td>
      <td> <?php echo ($product->getAmount()); ?></td>
      <td> <?php echo ($product->getPrice()); ?></td>
  	</tr>
    
 <?php }?>
  
  
</table>

</body>
</html>
